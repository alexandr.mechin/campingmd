import { createNativeStackNavigator, NativeStackScreenProps } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { enableScreens } from 'react-native-screens';
import HomePage from '../screens/Home';
import WelcomePage from '../screens/Welcome';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

enableScreens();

const MyStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name='Welcome' component={WelcomePage} options={{ headerShown: false }}></Stack.Screen>
      <Stack.Screen name='Home' component={HomePage} options={{ headerShown: false }}></Stack.Screen>
    </Stack.Navigator>
  );
};

export default MyStack;
