import 'styled-components/native';

declare module 'styled-components/native' {
  export interface DefaultTheme {
    colors: {
      white: string;
      orange: string;
      grey: string;
      whiteBlue: string;
      modalBackground: string;
      mainGreen: string;
    };
    borders: {
      borderRadius: string;
      pageBorderRadius: string;
    };
    shadows: {
      pageShadow: string;
    };
    paddings: {};
  }
}
