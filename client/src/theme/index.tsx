export const darkTheme = {
  colors: {
    white: 'white',
    orange: 'rgba(255, 119, 104, 1)',
    grey: 'rgba(0, 0, 0, 0.4)',
    whiteBlue: 'rgba(245, 247, 253, 1)',
    modalBackground: 'rgba(0, 0, 0, 0.2)',
    mainGreen: 'background: rgba(27, 77, 56, 1)',
  },
  borders: {
    borderRadius: '12px',
  },
  shadows: {
    pageShadow: '0 -2px 12px rgba(0, 0, 0, 0.25);',
  },
};

export const lightTheme = {
  colors: {
    white: 'white',
    orange: 'rgba(255, 119, 104, 1)',
    grey: 'rgba(0, 0, 0, 0.4)',
    whiteBlue: 'rgba(245, 247, 253, 1)',
    modalBackground: 'rgba(0, 0, 0, 0.2)',
    mainGreen: 'background: rgba(27, 77, 56, 1)',
  },
  borders: {
    borderRadius: '12px',
    pageBorderRadius: '20px',
  },
  shadows: {
    pageShadow: '0 -2px 12px rgba(0, 0, 0, 0.25);',
  },
};
