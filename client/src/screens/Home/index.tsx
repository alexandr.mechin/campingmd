import React from 'react';
import styled from 'styled-components/native';

import PageBg from '../../assets/AllPagesBg.png';
import OverlayView from '../../components/ui/OverlayView';
import FindNewPlace from './components/FindNewPlace';
import Recommended from './components/Recommended';
import Header from '../../components/modules/Header';

const Wrapper = styled.ImageBackground`
  height: 100%;
`;

const Page = styled.ScrollView`
  padding-top: 300px;
`;

const HomePage = () => {
  const children = (
    <>
      <FindNewPlace />
      <Recommended />
    </>
  );
  return (
    <Wrapper source={PageBg}>
      <Header />
      <Page showsVerticalScrollIndicator={false}>
        <OverlayView children={children} />
      </Page>
    </Wrapper>
  );
};

export default HomePage;
