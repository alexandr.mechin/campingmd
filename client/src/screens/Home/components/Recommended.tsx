import React from 'react';
import styled from 'styled-components/native';
import { SecondTitle } from '../../../components/ui/fonts';
import CampingItem from './CampingItem';

const Wrapper = styled.View`
  margin-top: 24px;
`;

const Campings = styled.ScrollView`
  margin-top: 20px;
`;

const Title = styled(SecondTitle)`
  padding-left: 16px;
`;

const campings = [
  {
    title: 'Днестр',
    location: 'Поселок Наславча',
    image: 'https://mediap.flypgs.com/awh/1254/836//files/Ekstrem_Sporlar/camping-kampcilik-nedir.jpg',
  },
  {
    title: 'Глемпинг',
    location: 'Город Бардар',
    image:
      'https://cf.bstatic.com/xdata/images/hotel/max1280x900/274637505.jpg?k=676e397072ddf0be73fa9821d4637a5617d230559acb485f99ee62fac45763a0&o=&hp=1',
  },
  {
    title: 'Кодры',
    location: 'Ниспоренский Район',
    image: 'https://media-cdn.tripadvisor.com/media/photo-s/15/a7/70/cf/camping-playa-taray.jpg',
  },
  {
    title: 'Днестр',
    location: 'Поселок Наславча',
    image: 'https://mediap.flypgs.com/awh/1254/836//files/Ekstrem_Sporlar/camping-kampcilik-nedir.jpg',
  },
];

const Recommended: React.FC<{}> = () => {
  return (
    <Wrapper>
      <Title weight='regular'>Рекоммендованые места</Title>
      <Campings horizontal showsHorizontalScrollIndicator={false}>
        {campings.map((item) => {
          return <CampingItem item={item} />;
        })}
      </Campings>
    </Wrapper>
  );
};

export default Recommended;
