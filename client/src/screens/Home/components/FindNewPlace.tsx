import React from 'react';
import styled from 'styled-components/native';
import { SmallText, Subtitle } from '../../../components/ui/fonts';
import RoundedButton from '../../../components/ui/RoundedButton';

const Wrapper = styled.TouchableOpacity`
  background-color: ${({ theme }) => theme.colors.whiteBlue};
  border-radius: ${({ theme }) => theme.borders.pageBorderRadius}
  flex-direction: row;
  padding:  10px;
  align-items: center;
  margin: 32px 16px 0px 16px;
  height: 75px;
`;

const TextWrapper = styled.View``;

const ButtonWrapper = styled.View`
  justify-content: center;
  align-items: flex-end;
  flex: 1;
`;

const FirstText = styled(Subtitle)``;
const SecondText = styled(SmallText)``;

const FindNewPlace: React.FC<{}> = () => {
  return (
    <Wrapper>
      <TextWrapper>
        <FirstText weight='bold'>Найти новое место для кемпинга</FirstText>
        <SecondText>Пора путешествовать, поищем лучшее место</SecondText>
      </TextWrapper>
      <ButtonWrapper>
        <RoundedButton type='primary' onPress={() => {}} />
      </ButtonWrapper>
    </Wrapper>
  );
};

export default FindNewPlace;
