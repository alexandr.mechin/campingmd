import React from 'react';
import styled from 'styled-components/native';
import { SmallText } from '../../../components/ui/fonts';

const Camping = styled.TouchableOpacity`
  border-radius: ${({ theme }) => theme.borders.borderRadius};
  height: 187px;
  width: 128px;
  margin-left: 16px;
  padding-bottom: 10px;
`;

const Wrapper = styled.Image`
  border-radius: ${({ theme }) => theme.borders.borderRadius};
  position: absolute;
  height: 187px;
  width: 128px;
`;

const Background = styled.View`
  border-radius: ${({ theme }) => theme.borders.borderRadius};
  background-color: ${({ theme }) => theme.colors.modalBackground};
  flex: 1;
  justify-content: flex-end;
  align-items: center;
`;

const Title = styled(SmallText)`
  color: ${({ theme }) => theme.colors.white};
  text-align: center;
`;

const Location = styled(SmallText)`
  color: ${({ theme }) => theme.colors.white};
`;

const CampingItem: React.FC<{}> = ({ item }) => {
  const { title, location, image } = item;
  return (
    <Camping>
      <Wrapper source={{ uri: image }} />
      <Background>
        <Title weight='bold'>{title}</Title>
        <Location>{location}</Location>
      </Background>
    </Camping>
  );
};

export default CampingItem;
