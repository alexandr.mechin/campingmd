import React from 'react';
import styled from 'styled-components/native';
import MainBg from '../../assets/MainBg.png';
import Button from '../../components/ui/Button';
import { Subtitle, Title } from '../../components/ui/fonts';
import MainLogo from '../../components/ui/MainLogo';

const Wrapper = styled.ImageBackground`
  height: 100%;
  flex: 1;
`;

const LogoWrapper = styled.View`
  align-items: center;
  margin-top: 300px;
`;

const TitleWrapper = styled.View`
  align-items: center;
  margin-top: 48px;
`;

const ButtonWrapper = styled.View`
  width: 100%;
  padding: 0 16px;
  position: absolute;
  bottom: 100px;
  align-items: center;
`;

const TitleText = styled(Title)`
  color: ${({ theme }) => theme.colors.white};
`;

const SubtitleText = styled(Subtitle)`
  color: ${({ theme }) => theme.colors.white};
  margin-top: 10px;
  text-align: center;
`;

const WelcomePage: React.FC<{ navigation: any }> = ({ navigation }) => {
  const enterApp = () => {
    navigation.navigate('Home');
  };
  return (
    <Wrapper source={MainBg}>
      <LogoWrapper>
        <MainLogo />
      </LogoWrapper>
      <TitleWrapper>
        <TitleText weight='bold'>КЕМПИНГ В МОЛДОВЕ</TitleText>
        <SubtitleText weight='bold'>Сообщество людей, для которых кемпинг это стиль жизни</SubtitleText>
      </TitleWrapper>
      <ButtonWrapper>
        <Button type='primary' text='Начать путешествие' onPress={enterApp} />
      </ButtonWrapper>
    </Wrapper>
  );
};

export default WelcomePage;
