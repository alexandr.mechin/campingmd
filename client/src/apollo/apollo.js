import { ApolloClient, HttpLink, InMemoryCache } from '@apollo/client';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setContext } from '@apollo/link-context';

const GRAPHQL_API_URL = 'http://localhost:5000/';

const asyncAuthLink = setContext(async () => {
  const TOKEN = await AsyncStorage.getItem('jwtToken');

  return {
    headers: {
      Authorization: TOKEN ? `Bearer ${TOKEN}` : '',
    },
  };
});

const httpLink = new HttpLink({
  uri: GRAPHQL_API_URL,
});

export const apolloClient = new ApolloClient({
  cache: new InMemoryCache(),
  // link: httpLink,
  link: asyncAuthLink.concat(httpLink),
});
