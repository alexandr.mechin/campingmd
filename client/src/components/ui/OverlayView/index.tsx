import React from 'react';
import { Dimensions } from 'react-native';
import styled from 'styled-components/native';

const windowHeight = Dimensions.get('window').height;

const Wrapper = styled.View`
  background-color: ${({ theme }) => theme.colors.white}
  border-radius: ${({ theme }) => theme.borders.pageBorderRadius}
  box-shadow: ${({ theme }) => theme.shadows.pageShadow}
  min-height: ${windowHeight - 300}px;
  width: 100%;
`;

const Header = styled.View`
  width: 100%;
  align-items: center;
  margin-top: 10px;
`;

const Tubler = styled.View`
  background-color: ${({ theme }) => theme.colors.grey}
  border-radius: ${({ theme }) => theme.borders.pageBorderRadius}
  height: 5px;
  width: 30px;
`;

const OverlayView: React.FC<{}> = ({ children }) => {
  return (
    <Wrapper>
      <Header>
        <Tubler />
      </Header>
      {children}
    </Wrapper>
  );
};

export default OverlayView;
