import React from 'react';
import styled from 'styled-components/native';
import { ButtonFont } from '../fonts';
import { AntDesign } from '@expo/vector-icons';
import { useTheme } from 'styled-components/native';

const ButtonWrapper = styled.TouchableOpacity<{ type: 'primary' | 'secondary' }>`
  background-color: ${({ theme, type }) => (type === 'primary' ? theme.colors.orange : theme.colors.orange)};
  border-radius: 190px;
  width: 40px;
  height: 40px;
  align-items: center;
  justify-content: center;
`;

type ButtonType = {
  type: 'primary' | 'secondary';
  onPress: () => void;
};

const RoundedButton: React.FC<ButtonType> = ({ type, onPress }) => {
  const theme = useTheme();
  console.log(theme);
  return (
    <ButtonWrapper type={type} onPress={onPress}>
      <AntDesign name='arrowright' size={24} color={theme.colors.white} />
    </ButtonWrapper>
  );
};

export default RoundedButton;
