import React from 'react';
import styled from 'styled-components/native';

const Font = styled.Text<{ weight?: 'light' | 'regular' | 'bold' }>`
  font-family: ${({ weight }) =>
    weight === 'regular' ? 'Bellota_400Regular' : weight === 'bold' ? 'Bellota_700Bold' : 'Bellota_300Light'};
`;

export const Title = styled(Font)`
  font-size: 26px;
`;

export const SecondTitle = styled(Font)`
  font-size: 24px;
`;

export const ButtonFont = styled(Font)`
  font-size: 18px;
`;

export const Subtitle = styled(Font)`
  font-size: 16px;
`;

export const SmallText = styled(Font)`
  font-size: 12px;
`;
