import React from 'react';
import styled from 'styled-components/native';
import LogoImage from '../../../assets/MainLogo.png';

const Wrapper = styled.View``;
const Logo = styled.Image`
  height: 100px;
  width: 100px;
`;

const MainLogo: React.FC<{}> = () => {
  return (
    <Wrapper>
      <Logo source={LogoImage} />
    </Wrapper>
  );
};

export default MainLogo;
