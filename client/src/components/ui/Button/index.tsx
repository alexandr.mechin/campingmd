import React from 'react';
import styled from 'styled-components/native';
import { ButtonFont } from '../fonts';

const ButtonWrapper = styled.TouchableOpacity<{ type: 'primary' | 'secondary' }>`
  background-color: ${({ theme, type }) => (type === 'primary' ? theme.colors.orange : theme.colors.orange)};
  border-radius: ${({ theme }) => theme.borders.borderRadius}
  width: 100%;
  height: 40px;
  align-items: center;
  justify-content: center;
`;

const ButtonText = styled(ButtonFont)<{ type: 'primary' | 'secondary' }>`
  color: ${({ theme, type }) => (type === 'primary' ? theme.colors.white : theme.colors.orange)};
`;

type ButtonType = {
  text: string;
  type: 'primary' | 'secondary';
  onPress: () => void;
};

const Button: React.FC<ButtonType> = ({ text, type, onPress }) => {
  return (
    <ButtonWrapper type={type} onPress={onPress}>
      <ButtonText type={type} weight='bold'>
        {text}
      </ButtonText>
    </ButtonWrapper>
  );
};

export default Button;
