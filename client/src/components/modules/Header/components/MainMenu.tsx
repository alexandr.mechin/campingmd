import React from 'react';
import styled from 'styled-components/native';
import { Modal } from 'react-native';

import { AntDesign } from '@expo/vector-icons';

const Wrapper = styled.View`
  background-color: ${({ theme }) => theme.colors.modalBackground};
  width: 100%
  height: 100%;
`;

const Content = styled.View`
  background-color: ${({ theme }) => theme.colors.whiteBlue};
  padding: 50px 16px 16px 16px;
  border-radius: 20px;
  height: 100%;
  width: 60%;
`;

const CloseMenu = styled.TouchableOpacity`
  align-items: flex-end;
`;

type Props = {
  visibility: boolean;
  setVisibility: (visibility: boolean) => void;
};

const MainMenu: React.FC<Props> = ({ visibility, setVisibility }) => {
  return (
    <Modal transparent visible={visibility} animationType='fade'>
      <Wrapper>
        <Content>
          <CloseMenu onPress={() => setVisibility(false)}>
            <AntDesign name='menufold' size={24} color='black' />
          </CloseMenu>
        </Content>
      </Wrapper>
    </Modal>
  );
};

export default MainMenu;
