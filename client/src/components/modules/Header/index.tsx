import React, { useState } from 'react';
import styled from 'styled-components/native';

import { Feather } from '@expo/vector-icons';
import MainMenu from './components/MainMenu';

const Wrapper = styled.View`
  position: absolute;
  top: 50px;
  padding: 0 16px;
  flex-direction: row;
  justify-content: space-between
  align-items: center;
  height: 40px;
  width: 100%;
  z-index: 1;
`;

const MenuWrapper = styled.TouchableOpacity``;
const SettingsWrapper = styled.TouchableOpacity``;

const MenuIcon = styled(Feather)``;

const Header: React.FC<{}> = () => {
  const [menuVisibility, setMenuVisibility] = useState(false);
  const handleOpenMenu = () => {
    setMenuVisibility(true);
  };
  const handleOpenSettings = () => {};

  return (
    <Wrapper>
      <MenuWrapper onPress={handleOpenMenu}>
        <MenuIcon name='menu' size={24} color='white' />
      </MenuWrapper>
      <SettingsWrapper onPress={handleOpenSettings}>
        <MenuIcon name='settings' size={24} color='white' />
      </SettingsWrapper>
      <MainMenu visibility={menuVisibility} setVisibility={setMenuVisibility} />
    </Wrapper>
  );
};

export default Header;
