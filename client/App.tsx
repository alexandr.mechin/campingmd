import React from 'react';
import { ThemeProvider } from 'styled-components/native';

import { ApolloProvider } from '@apollo/client';
import { apolloClient } from './src/apollo/apollo';

import { NavigationContainer } from '@react-navigation/native';
import Navigator from './src/navigator';

import AppLoading from 'expo-app-loading';
import {
  useFonts,
  Bellota_300Light,
  Bellota_300Light_Italic,
  Bellota_400Regular,
  Bellota_400Regular_Italic,
  Bellota_700Bold,
  Bellota_700Bold_Italic,
} from '@expo-google-fonts/bellota';

import { useColorScheme } from 'react-native';
import { darkTheme, lightTheme } from './src/theme';

export default function App() {
  const isDarkMode = useColorScheme() === 'dark';
  console.log(isDarkMode);
  const theme = isDarkMode ? darkTheme : lightTheme;

  let [fontsLoaded] = useFonts({
    Bellota_300Light,
    Bellota_300Light_Italic,
    Bellota_400Regular,
    Bellota_400Regular_Italic,
    Bellota_700Bold,
    Bellota_700Bold_Italic,
  });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <ApolloProvider client={apolloClient}>
      <ThemeProvider theme={theme}>
        <NavigationContainer>
          <Navigator />
          {/* <RootComponent /> */}
        </NavigationContainer>
      </ThemeProvider>
    </ApolloProvider>
  );
}
