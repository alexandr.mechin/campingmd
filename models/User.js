const { model, Schema } = require('mongoose');

const userSchema = new Schema({
  username: String,
  fullname: String,
  password: String,
  email: String,
  createdAt: String,
  photoUrl: String,
  role: String,
});

module.exports = model('User', userSchema);
