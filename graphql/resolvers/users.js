const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { UserInputError, AuthenticationError } = require('apollo-server');
const checkAuth = require('../../util/check-auth');
const { validateRegisterInput, validateLoginInput } = require('../../util/validators');
const { SECRET_KEY } = require('../../config');
const User = require('../../models/User.js');

function generateToken(user) {
  return jwt.sign(
    {
      id: user.id,
      email: user.email,
      username: user.username,
      fullname: user.fullname,
      photoUrl: user.photoUrl,
      role: user.role,
    },
    SECRET_KEY,
    { expiresIn: '5h' }
  );
}

module.exports = {
  Query: {
    async getUsers() {
      try {
        const users = await User.find().sort({ createdAt: -1 });
        return users;
      } catch (err) {
        throw new Error(err);
      }
    },
    async getUser(_, { userId }) {
      try {
        const user = await User.findById(userId);
        if (user) {
          return user;
        } else {
          throw new Error('User not found');
        }
      } catch (err) {
        throw new Error(err);
      }
    },
  },
  Mutation: {
    async login(_, { username, password }) {
      const { errors, valid } = validateLoginInput(username, password);
      if (!valid) {
        throw new UserInputError('Errors', { errors });
      }

      const user = await User.findOne({ username });

      if (!user) {
        errors.general = 'User not found';
        throw new UserInputError('User not found', { errors });
      }

      const match = await bcrypt.compare(password, user.password);
      if (!match) {
        errors.general = 'Wrong credentials';
        throw new UserInputError('Wrong credentials', { errors });
      }
      const token = generateToken(user);

      return {
        ...user._doc,
        id: user._id,
        token,
      };
    },
    async register(_, { registerInput: { username, fullname, email, password, confirmPassword, photoUrl, role } }) {
      const { valid, errors } = validateRegisterInput(username, fullname, email, password, confirmPassword, photoUrl, role);
      if (!valid) {
        throw new UserInputError('Errors', { errors });
      }
      // Make sure user doesnt already exist
      const user = await User.findOne({ username });
      if (user) {
        throw new UserInputError('Username is taken', {
          errors: {
            username: 'This username is taken',
          },
        });
      }

      //Hash password and create an auth token
      password = await bcrypt.hash(password, 12);

      const newUser = new User({
        email,
        username,
        fullname,
        password,
        createdAt: new Date().toISOString(),
        photoUrl,
        role: 'client',
      });

      const res = await newUser.save();

      const token = generateToken(res);
      return {
        ...res._doc,
        id: res._id,
        token,
      };
    },
    async deleteUser(_, { userId }, context) {
      const currentUser = checkAuth(context);
      if (currentUser.role === 'admin') {
        try {
          const user = await User.findById(userId);
          if (user.id !== currentUser.id) {
            await user.delete();
            return 'User deleted successfully';
          } else {
            throw new AuthenticationError("You can't delete yourself");
          }
        } catch (err) {
          throw new Error(err);
        }
      } else {
        throw new AuthenticationError('Only admins can delete Users');
      }
    },
    async updateUser(_, { userId, fullname, email, role, photoUrl }, context) {
      const currentUser = checkAuth(context);
      if (currentUser.role === 'admin') {
        try {
          const user = await User.findByIdAndUpdate(userId, {
            fullname,
            email,
            role,
            photoUrl,
          });
          return user;
        } catch (err) {
          throw new Error(err);
        }
      }
    },
  },
};
